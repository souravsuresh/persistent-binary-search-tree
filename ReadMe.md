## About:

This project creates a persistent binary search tree using C++ generics.(The data about the tree is stored in a file which can be used Later).


## Where is this useful?

When we have memory contraints due to overusage(less RAM) , we can store the entire data  in secondary memory in a file and completely operate on the file itself to avoid any overhead in the RAM.
In this way , we save memory and keep the program running.

## Structure of the code

This project contains 2 files.
1. final_bin_gen.cpp		- This file creates the binary search tree and writes data in a particular format into the file.
						-  Also provides an interface for iterating the binary search tree.
						  
2. bt_str_10				- This file stores the binary search tree.




## Execution

1. cat > bt_str_10
		  1000 ~ ~ 
		  (Press ctrl-c)

2. Execute final_bin_gen.cpp
3. ./a.out
4. :+1:
		(ALWAYS ENSURE THAT cat COMMAND(1.) IS USED BEFORE EVERY EXECUTION)