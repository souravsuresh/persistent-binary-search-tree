#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <sstream>
#include <stack>
#include <algorithm>
#include <string>
#include <cstring>
#include <map>
#include <list>
using namespace std;

template<typename T>
class BinaryTree
{
	private:
        struct node
        {
            string data;
            string left;
            string right;
            node(string s1,string s2,string s3)
            {
                data=s1;
                left=s2;
                right=s3;
            }

        };
        fstream f;
	    public:
            BinaryTree()
            {
                f.open("bt_str_10");
            }
            void insert(T info);
        
		class iterator : public std::iterator<std::input_iterator_tag, typename BinaryTree::node>
		{
			private:
                
				std::stack<node*> s;
				int curr= 0;
				int nbytes = 50;
                string data,left,right;
                string curr_sp;
			public:
               
                node* current;
				iterator(node*  n=nullptr)
				{
                    fstream f;
                    f.open("bt_str_10");
                    f.seekg(0);
                    f>>data>>left>>right;
                    right.erase(right.find('`'));
                    if(n==nullptr)
    					current=new node(data,left,right); //initializing the root node...
                    else
                        current=n;
                    s.push(current);
                    curr_sp=s.top()->left;
				}
                iterator& operator++()
                {
                    int temp;
                    fstream f("bt_str_10");
                    while(curr_sp!="-1")
                    {
                        stringstream convert(curr_sp);
                        convert>>temp;
                        f.seekg(temp*50);
                        f>>data>>left>>right;
                        right.erase(right.find('`'));
                        s.push(new node(data,left,right));
                        curr_sp=left;
                    }
                    if(s.size())
                    {
                        node* top=s.top();
                        current=top;
                        s.pop();
                        curr_sp=top->right;
                    }
                    else
                        current=new node("$$"," "," ");
                    return *this;
                }
				T operator*() 
				{
					return current->data;
				}
				bool operator==(const iterator& rhs) 
				{
					return current->data==rhs.current->data;
				}
                bool operator!=(const iterator& rhs) 
				{
					return current->data!=rhs.current->data;
				}
		};

		auto begin()
		{
            f.seekg(0);
            string d,l,r;
            f>>d>>l>>r;
            r.erase(r.find('`'));
			return (iterator(new node(d,l,r)));
		}
        auto end()
        {
           return iterator(new node("$$"," "," "));
        }

};

template<typename T>
void BinaryTree<T>::insert(T dat)
{	
	string data;
	string left;
	string right;
	int curr=0;
    f.seekg(0);
	while(f>>data>>left>>right)
	{
		T da;
		
		char* wr=(char*)(malloc(sizeof(char)*51));
		stringstream convert(data);     //converting the data to user defiend value type T
		convert>>da;
		if(da==1000 && (left=="~") && (right=="~"))	//root node
		{
					
					int off=0;
					f.seekg(0);          
                    string t;
                   
                    t =to_string(dat) + " -1" + " -1";
					strncpy(wr, t.c_str(), t.size());
					for (int i = strlen(wr); i < 50; i++)
					    wr[i] = '`';
					wr[49]='\n';
					char* temp_str=(char*)(malloc(sizeof(char)*51));
					for (int i = 0; i < 50; i++)
					    temp_str[i] = '`';
					temp_str[49]='\n';
                    f << wr;
					f.seekg(((2 * off + 1) * 50));
					f << temp_str;
					f.seekg(((2 * off + 2) * 50));
					f << temp_str;
					break;

		}
		else if(dat>da)
		{
			//checking right part of the sub tree....
			int temp;
			stringstream convert(right);
			convert>>temp;
			if(temp!=-1)
			{
				f.seekg(temp*50-1);
				curr=temp;
			}
			else
			{
				int off=2*(curr)+2;
				string temp;
				f.seekg(curr*50);
				char* tem_s=(char*)(malloc(sizeof(char)*51));
				string tem=data+" "+(left)+" "+to_string((2*(curr)+2));
				strncpy(tem_s, tem.c_str(), tem.size());
				for(int i=tem.size();i<50;++i)
					tem_s[i]='`';
				tem_s[49]='\n';
				f<<tem_s;			
				f.seekg((2*(curr)+2)*50);
                string t;
    				t =to_string(dat) + " -1" + " -1";
 
    			strncpy(wr, t.c_str(), t.size());
				for (int i = strlen(wr); i < 50; i++)
				    wr[i] = '`';
				wr[49] = '\n';
				char *temp_str = (char *)(malloc(sizeof(char) * 51));
				for (int i = 0; i < 50; i++)
				    temp_str[i] = '`';
				temp_str[49] = '\n';
				
				f << wr;
				f.seekg(((2 * off + 1) * 50));
				f << temp_str;
				f.seekg(((2 * off + 2) * 50));
				f << temp_str;
				break;
			}
		}
		else
		{
			int temp;
			stringstream convert(left);
			convert>>temp;
			
			if(temp!=-1)
			{
				
				f.seekg(temp*50-1);
				curr=temp;
				
			}
			else
			{
				int off=2*(curr)+1;
				string temp;
				f.seekg(curr*50);
				char* tem_s=(char*)(malloc(sizeof(char)*51));
				string tem=data+" "+to_string((2*(curr)+1))+" "+right;
				strncpy(tem_s, tem.c_str(), tem.size());
				for(int i=tem.size();i<50;++i)
					tem_s[i]='`';
				tem_s[49]='\n';
				f<<tem_s;			
				f.seekg((2*(curr)+1)*50);
				string t;
    				t =to_string(dat) + " -1" + " -1";

    			strncpy(wr, t.c_str(), t.size());
				for (int i = strlen(wr); i < 50; i++)
				    wr[i] = '`';
				wr[49] = '\n';

				char *temp_str = (char *)(malloc(sizeof(char) * 51));
				for (int i = 0; i < 50; i++)
				    temp_str[i] = '`';
				temp_str[49] = '\n';

				f << wr;
				f.seekg(((2 * off + 1) * 50));
				f << temp_str;
				f.seekg(((2 * off + 2) * 50));
				f << temp_str;
				break;
			}

		}
	}

}
template<>
void BinaryTree<string>::insert(string dat)
{	
    
	string data;
	string left;
	string right;
	int curr=0;
    f.seekg(0);
	while(f>>data>>left>>right)
	{
		string da;
		
		char* wr=(char*)(malloc(sizeof(char)*51));
		stringstream convert(data);     //converting the data to user defiend value type T
		convert>>da;
		if(da=="1000" && (left=="~") && (right=="~"))	//root node
		{
					int off=0;
					f.seekg(0);          
                    string t;
                    t =(dat) + " -1" + " -1";
        			strncpy(wr, t.c_str(), t.size());
					for (int i = strlen(wr); i < 50; i++)
					    wr[i] = '`';
					wr[49]='\n';
					char* temp_str=(char*)(malloc(sizeof(char)*51));
					for (int i = 0; i < 50; i++)
					    temp_str[i] = '`';
					temp_str[49]='\n';
					//writing to the file
                    f << wr;
                    //creates and inserts child of the current node inserted in the tree
					f.seekg(((2 * off + 1) * 50));
					f << temp_str;
					f.seekg(((2 * off + 2) * 50));
					f << temp_str;
					break;

		}
		else if(dat>da)
		{
			//checking right part of the sub tree....
			int temp;
			stringstream convert(right);
			convert>>temp;
			if(temp!=-1)
			{
				f.seekg(temp*50-1);
				curr=temp;
			}
			else
			{
				int off=2*(curr)+2;
				string temp;
				f.seekg(curr*50);
				char* tem_s=(char*)(malloc(sizeof(char)*51));
				string tem=data+" "+(left)+" "+to_string((2*(curr)+2));
				strncpy(tem_s, tem.c_str(), tem.size());
				for(int i=tem.size();i<50;++i)
					tem_s[i]='`';
				tem_s[49]='\n';
				f<<tem_s;			
				f.seekg((2*(curr)+2)*50);
                string t;
                t =(dat) + " -1" + " -1";
                strncpy(wr, t.c_str(), t.size());
				for (int i = strlen(wr); i < 50; i++)
				    wr[i] = '`';
				wr[49] = '\n';
				char *temp_str = (char *)(malloc(sizeof(char) * 51));
				for (int i = 0; i < 50; i++)
				    temp_str[i] = '`';
				temp_str[49] = '\n';
				f << wr;
				f.seekg(((2 * off + 1) * 50));
				f << temp_str;
				f.seekg(((2 * off + 2) * 50));
				f << temp_str;
				break;
			}
		}
		else
		{
			int temp;
			stringstream convert(left);
			convert>>temp;
			if(temp!=-1)
			{	
				f.seekg(temp*50-1);
				curr=temp;	
			}
			else
			{
				int off=2*(curr)+1;
				string temp;
				f.seekg(curr*50);
				char* tem_s=(char*)(malloc(sizeof(char)*51));
				string tem=data+" "+to_string((2*(curr)+1))+" "+right;
				strncpy(tem_s, tem.c_str(), tem.size());
				for(int i=tem.size();i<50;++i)
					tem_s[i]='`';
				tem_s[49]='\n';
				
                f<<tem_s;			
				f.seekg((2*(curr)+1)*50);
				string t;
                t =(dat) + " -1" + " -1";
                strncpy(wr, t.c_str(), t.size());
				for (int i = strlen(wr); i < 50; i++)
				    wr[i] = '`';
				wr[49] = '\n';
				char *temp_str = (char *)(malloc(sizeof(char) * 51));
				for (int i = 0; i < 50; i++)
				    temp_str[i] = '`';
				temp_str[49] = '\n';
				f << wr;
				f.seekg(((2 * off + 1) * 50));
				f << temp_str;
				f.seekg(((2 * off + 2) * 50));
				f << temp_str;
				break;
			}
		}
	}
}
template<typename ptr_t>
void disp(ptr_t f,ptr_t l)
{
    while(f!=l)
    {
        cout<<*f<<endl;
        ++f;
    }

}
template<typename ptr_t>
ptr_t mymax(ptr_t first,ptr_t l)
{
    ptr_t m=first;
    while(first!=l)
    {
        
        if(*m<*first)
            m=first;
        ++first;
    }
    return m;
}
template <typename T>
bool pred_bin(T ele)
{
    int temp;
    stringstream s(ele);
    s>>temp;
    return temp%10==0;
}
struct cmpr
{
    private:
        map<string,list<string>> team;
        string current_team;
    public:
        cmpr(string c)
        {
            current_team=c;
            team["RCB"].push_back("kholi");
            team["RCB"].push_back("abd");
            team["RCB"].push_back("gayle");
            team["RCB"].push_back("mills");
            team["RPS"].push_back("zampa");
        }
        bool operator()(string ele)
        {
            auto it=team[current_team].begin();
            if(find(team[current_team].begin(),team[current_team].end(),ele)!=team[current_team].end())
                    return true;
            return false;
        }

};
cmpr mycmp(string team_name)
{
    return cmpr(team_name);
}

int main()
{
	BinaryTree<string> b;
	
    //Insertion ....

    b.insert("kholi");
    b.insert("abd");
    b.insert("gayle");
    b.insert("stokes");
    b.insert("mills");
    b.insert("zampa");
    b.insert("warner");
    
    //ALGORITHMS 

    // disp(b.begin(),b.end());

    //1
    cout<<"for_each"<<endl;
    cout<<"\t";
    for_each(++b.begin(),b.end(),[](auto ele){cout<<ele<<"\t";});
    cout<<"\n";
   
   //not working
   cout<<"my max_element"<<endl;
   cout<<"\t"<<*(mymax(b.begin(),b.end()))<<endl;

    //2
    cout<<"min_element"<<endl;
    cout<<"\t"<<*(min_element(b.begin(),b.end()))<<endl;

    //3
    cout<<"find"<<endl;
    auto it4=find(b.begin(),b.end(),"kholi");
    if(it4!=b.end())
        cout<<"\t"<<"found"<<endl;
    else
        cout<<"\t"<<"not found"<<endl;
    it4=find(b.begin(),b.end(),"dhoni");
    if(it4!=b.end())
        cout<<"\t"<<"found"<<endl;
    else
        cout<<"\t"<<"not found"<<endl;
    
    //4
    cout<<"count"<<endl;
    cout<<"\t"<<count(b.begin(),b.end(),"kholi")<<endl;

    //5
    cout<<"find_if"<<endl;    
    cout<<"\t"<<*(find_if(b.begin(),b.end(),mycmp("RCB")))<<endl;

    //6
    cout<<"find_if_not"<<endl;    
    cout<<"\t"<<*(find_if_not(++b.begin(),b.end(),mycmp("RCB")))<<endl;
    
    //check
    // //7
    cout<<"is_sorted"<<endl;    
    auto it=(is_sorted(++b.begin(),b.end()));
    cout<<"\t"<<boolalpha<<it<<endl;
    
    
    //auto ret=accumulate(b.begin(),b.end(),"0");
    //cout<<ret<<endl;
    // auto itt=b.begin();
    
}
